import './App.css';
import SectionContainer from './components/SectionContainer'
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import {
  useState, useEffect
} from 'react';
import PresenterControls from './components/headers/PresenterControls'

const initData = {
  items: {
    'item-1':{
      title: 'Slides Control',
      content: <div>Slides 1</div>,
      className: [],
      isOpen: false
    },
    'item-2':{
      title: 'Live Preview',
      content: 'What your viewer sees',
      className: [],
      isOpen: false
    },
    'item-3':{
      title: 'Webcam',
      content: 'Your Dolby Cam',
      className: [],
      isOpen: true,
      allowedContainers: ['container-3']
    },
    'item-4':{
      title: 'Producer Controls',
      content: 'Some Settings',
      className: [],
      isOpen: false
    },
    'item-5':{
      title: 'Presenter Chat',
      content: 'Chat Messages',
      className: [],
      isOpen: false
    },
    'item-6':{
      title: 'Questions',
      content: 'Some Questions',
      className: [],
      isOpen: false
    },
    'item-7':{
      title: 'Polls',
      content: 'Poll 1',
      className: [],
      isOpen: false
    },
    'item-8':{
      title: 'Playlist',
      content: 'Video 1',
      className: [],
      isOpen: false
    },
    'item-9':{
      title: 'Audience View Options',
      content: 'Some Options',
      className: [],
      isOpen: false
    },
    'item-10':{
      title: 'Audience Alerts',
      content: 'Alerts Goes here',
      className: [],
      isOpen: false
    }
  },
  containers: {
    'container-1':{
      id: 'container-1',
      className: ['primary-view'],
      direction: 'vertical',
      minItem: 1,
      maxItem: 1,
      items: ['item-1'],
      noPlace: true,
      isFocus: false,
      mobile: true
    },
    'container-2':{
      id: 'container-2',
      className: ['secondary-view-container'],
      direction: 'horizontal',
      minItem: 1,
      maxItem: 3,
      items: ['item-2'],
      isFocus: false,
      mobile: false
    },
    'container-3':{
      id: 'container-3',
      className: ['sidebar'],
      direction: 'vertical',
      minItem: 1,
      maxItem: 10,
      items: ['item-3', 'item-4', 'item-5', 'item-6', 'item-7', 'item-8', 'item-9', 'item-10'],
      isFocus: false,
      mobile: true
    }
  },
  containerIndex: ['container-1', 'container-2', 'container-3']
}

function App() {
  const [layoutData, setLayoutData] = useState(initData);

  const onBeforeCapture = eventData => {
    //console.log(eventData);
    const {draggableId} = eventData;
    const tempData = {...layoutData};
    tempData.items[draggableId].className.push('onDrag');
    setLayoutData(tempData);
  }

  const onDragUpdate = eventData => {
    if(!eventData.destination){
      return
    }
    //console.log(eventData);
    const tempData = {...layoutData};
    for (const key in tempData.containers) {
      tempData.containers[key].isFocus = false;
    }
    tempData.containers[eventData.destination.droppableId].isFocus = true;

    const item = tempData.items[eventData.draggableId];
    if(item.allowedContainers){
      const allowedContainers = item.allowedContainers;
      // console.log(allowedContainers);
      // console.log(eventData.destination.droppableId);
      if(!allowedContainers.includes(eventData.destination.droppableId)){
        //console.log('Not Allowed');
        if(!tempData.items[eventData.draggableId].className.includes('notAllowed')){
          tempData.items[eventData.draggableId].className.push('notAllowed');
        }
      }else{
        if(tempData.items[eventData.draggableId].className.includes('notAllowed')){
          tempData.items[eventData.draggableId].className.pop();
        }
      }
    }

    setLayoutData(tempData);
  }

  const onDragEnd = result => {
    //console.log(result);
    const {destination, source, draggableId} = result;
    const tempData = {...layoutData};
    for (const key in tempData.containers) {
      tempData.containers[key].isFocus = false;
    }
    tempData.items[draggableId].className = [];
    if(!destination){
      return
    }else if(destination.droppableId === source.droppableId && destination.index === source.index){
      return
    }else{
      if(destination.droppableId === source.droppableId){
        const items = tempData.containers[source.droppableId].items;
        items.splice(source.index, 1);
        items.splice(destination.index, 0, draggableId);
        
      }else if(destination.droppableId !== source.droppableId){
        const sourceContainer = tempData.containers[source.droppableId];
        const destinationContainer = tempData.containers[destination.droppableId];
        if(tempData.items[draggableId].allowedContainers){
          if(!tempData.items[draggableId].allowedContainers.includes(destination.droppableId)){
            return
          }
        }
        if(destinationContainer.items.length >= destinationContainer.maxItem || sourceContainer.items.length <= sourceContainer.minItem){
          console.log('swap');
          const swapItem = destinationContainer.items[destination.index] || destinationContainer.items[destination.index-1];
          const destinationIndex = destinationContainer.items[destination.index] ? destination.index : destination.index-1;
          if(tempData.items[swapItem].allowedContainers){
            //if destination item allow
            if(tempData.items[swapItem].allowedContainers.includes(source.droppableId)){
              destinationContainer.items.splice(destinationIndex, 1, draggableId);
              sourceContainer.items.splice(source.index, 1, swapItem);
            }else{
              sourceContainer.items.splice(source.index, 1);
              destinationContainer.items.splice(destination.index, 0, draggableId);
            }
          }else{
            destinationContainer.items.splice(destinationIndex, 1, draggableId);
            sourceContainer.items.splice(source.index, 1, swapItem);
          }
        }else{
          console.log('move');
          sourceContainer.items.splice(source.index, 1);
          destinationContainer.items.splice(destination.index, 0, draggableId);
        }
      }
      setLayoutData(tempData);
    }
  };

  const allItemClose = (target) => {
    const tempData = {...layoutData};
    if(tempData.items[target].isOpen === true){
      tempData.items[target].isOpen = false;
    }else{
      for (const key in tempData.items) {
        tempData.items[key].isOpen = false;
      }
      tempData.items[target].isOpen = true;
    }
    setLayoutData(tempData);
  }

  useEffect(() => {
    const handleResize = () => {
      //console.log('Width is smaller than 800px?', window.innerWidth < 800);
      if(window.innerWidth < 800){
        const tempData = {...layoutData};
        let tempItems = [];
        for(const key in tempData.containers){
          if(!tempData.containers[key].mobile){
            tempItems = [...tempData.containers[key].items]
            tempData.containers[key].items = []
          }
        }
        const newArray = tempData.containers['container-3'].items.concat(tempItems);
        tempData.containers['container-3'].items = newArray;
        setLayoutData(tempData);
      }else{

      }
    }
    window.addEventListener('resize', handleResize);
    return () => {
      return () => window.removeEventListener("resize", handleResize);
    }
  }, [])

  return (
    <DragDropContext
      onBeforeCapture={onBeforeCapture}
      onDragEnd={onDragEnd}
      onDragUpdate={onDragUpdate}
    >
      <div className="App">
        <header className='header'>
          <img className='header-logo' src='https://streamgo.co.uk/wp-content/uploads/2018/12/cropped-GO-BiG-SQUARE-GREEN-192x192.png' alt='ico'/>
          <h2>PresenterGo</h2>
          <div className='header-widgets'>
            <PresenterControls/>
          </div>
        </header>
        <div id='main' className='main'>
          {layoutData.containerIndex.map(index => {
            return (
              <Droppable droppableId={layoutData.containers[index].id} key={layoutData.containers[index].id} direction={layoutData.containers[index].direction}>
                {provided=>(
                  <div {...provided.droppableProps} ref={provided.innerRef} key={layoutData.containers[index].id} id={layoutData.containers[index].id} className={`viewContainer `+layoutData.containers[index].className.join(' ')+` ${layoutData.containers[index].isFocus ? 'isFocus' : ''}`}>
                  {
                    layoutData.containers[index].items.map((item, index) => {
                      return(
                        <SectionContainer 
                        key={item} 
                        id={item} 
                        index={index} 
                        title={layoutData.items[item].title} 
                        className={layoutData.items[item].className.join(' ')} 
                        defaultOpen={index === 0}
                        isOpen={layoutData.items[item].isOpen}
                        openHandler={()=>allItemClose(item)}
                        >
                          {layoutData.items[item].content}
                        </SectionContainer>
                      )
                    })
                  }
                  {provided.placeholder}
                  </div>
                )}
              </Droppable>
            )
          })}
        </div>
      </div>
    </DragDropContext>
  );
}

export default App;
