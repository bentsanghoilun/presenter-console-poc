import React, { useState, useEffect} from 'react'
import Moment from 'react-moment'
import { 
    FiMicOff,
    FiMic,
    FiVideoOff,
    FiLink,
    FiUsers,
    FiClock,
    FiLogOut,
    FiPauseCircle,
    FiPlay
} from "react-icons/fi";

const PresenterControls = props => {
    const initStreamStatus = {
        streaming: false,
        streamTime: 0
    }
    const [streamStatus, setstreamStatus] = useState(initStreamStatus);
    const toggleStream = () =>{
        const newStreamStatus = {...streamStatus}
        if(newStreamStatus.streaming){

        }else{
            
        }
        newStreamStatus.streaming = !newStreamStatus.streaming
        //console.log(newStreamStatus)
        setstreamStatus(newStreamStatus)
    }
    return(
        <React.Fragment>
            <div 
                className={`widget-btn clickable ${streamStatus.streaming ? 'animation-streaming' : ''}`}
                onClick={toggleStream}
            >
                {streamStatus.streaming ? <FiPauseCircle/>:<FiPlay/>}
                &nbsp;{
                    streamStatus.streaming ? 
                    <React.Fragment>
                        <span>Streaming:&nbsp;</span>
                        <Moment date={streamStatus.streamTime} format="hh:mm:ss" durationFromNow />
                    </React.Fragment>
                    :'Stopped'
                }
            </div>
            
            <div className='widget-btn clickable'><FiMic/></div>
            <div className='widget-btn clickable'><FiVideoOff/></div>
            <div className='divider'></div>
            <div className='widget-btn clickable'><FiLink/> &nbsp; Invite Speakers</div>
            <div className='widget-btn clickable'><FiUsers/>&nbsp;&nbsp;243</div>
            <div className='widget-btn'><FiClock/>&nbsp;<Moment interval={1000} format='hh:mm A'></Moment></div>
            <div className='divider'></div>
            <div className='widget-btn clickable'><FiLogOut/></div>
        </React.Fragment>
    )
}

export default PresenterControls;