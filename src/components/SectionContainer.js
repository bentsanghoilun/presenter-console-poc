import './SectionContainer.css'
import { Draggable } from 'react-beautiful-dnd';
import {
    useState, useEffect
  } from 'react';
import { FiPlus, FiMinus } from "react-icons/fi";
import { FaBan } from "react-icons/fa";


const SectionContainer = props => {
    //const [isOpen, setIsOpen] = useState(props.isOpen);

    return(
        <Draggable draggableId={props.id} index={props.index} key={props.id}>
        {(provided) => (
            <div 
            {...provided.draggableProps}
            
            ref={provided.innerRef}
            key={props.id}
            className={`sectionContainer ${props.className}`}>
                <div className='title' {...provided.dragHandleProps}>
                    {props.title}
                    <span className='expandIcon' onClick={props.openHandler}>
                        {props.isOpen ? <FiMinus/> : <FiPlus/>}
                    </span>
                </div>
                <div className={`container ${props.isOpen ? 'isOpen' : ''}`}>{props.children}</div>
                <div className='notAllowDropIcon'>
                    <FaBan/>
                </div>
            </div>
        )}
        </Draggable>
    )
}
export default SectionContainer;